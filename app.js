// 
// File: app.js
// Auth: Martin Burolla
// Date: 12/06/2022
// Desc: Simple Express Web API.
//

const cors = require('cors')
const express = require('express')

const PORT = 80
const app = express()
const corsOptions = { origin: ['http://localhost:3000'], optionsSuccessStatus: 200 }

// Middleware...
app.use(cors())
app.use(express.json())
app.use(express.urlencoded())

//
// GET /message
//

app.get('/message', cors(corsOptions), async (req, res) => { 
    // let result = await dataAccess. <YOUR FUNCTION HERE>
    // let id = req.params['id'];                 // Read params from URL.
    // let queryParam1 = req.query['personType']  // Read query params from URL.
    // let body = req.body;                       // Read request body.
    // res.send(<YOUR OBJECT HERE>);
    res.send({message: 'Hello World'});
});

app.listen(PORT, () => {
    console.log(`Express Web API running on port: ${PORT}`);
})