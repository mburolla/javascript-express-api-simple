# Simple Express API
Used for testing CI/CD, runs on port 80.

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run the API: `npm start` or `node app.js`

# Endpoints
- `GET /message`
